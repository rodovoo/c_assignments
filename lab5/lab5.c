#include <stdio.h>
int main() { 
    
    int students = 0 ; //จำนวนนักเรียน
    char name[20][20] ; //ชื่อนักเรียน
    int exam[20] ; //คะแนนข้อสอบ
    int i = 0 ; //
    int max = 0 ; //รับค่าคะแนนที่เยอะ
    int low = 100 ; //รับค่าตะแนนที่น้อย
    int indexMax ; //คะแนนที่เยอะที่สุดของทุกคน
    int indexMin ; //คะแนนที่น้อยที่สุดของทุกคน
    
    //รับจำนวนนักเรียน
    printf("The number of students in the classroom\n");
    scanf("%d",&students);

    //นักเรียนไม่เกิน20
    if (students<=20) {
        printf("You have the number of students in the room %d.\n",students);  
        //รับค่าคะแนนของนักเรียนทุกคน
        for (i=0;i<students;i++) {
            printf("Student name ? \n");
            scanf("%s",&name[i]);
            printf("Student score\n");
            scanf("%d",&exam[i]);
            //หาคะแนนที่เยอะที่สุด
            if (exam[i]>max) { 
                max=exam[i];
                indexMax=i;
            }
            //หาคะแนนน้อยสุด
            if (exam[i]<=low) {
                low=exam[i];            
                indexMin=i;
            }
        }
        //แสดงคะแนนมากและน้อยท่สุด
        printf("%s had the highest grade in the class %d\n",name[indexMax],max);
        printf("%s had the low grade in the class %d\n",name[indexMin],low);
    }

    //นักเรียนเกิน20
    else {
        printf("There are too many students\n");
    }
         return  0 ;
}   